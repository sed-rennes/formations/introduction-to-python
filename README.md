# Basic introduction to Python 

This is a basic tutorial to discover Python langage
version 3.x 

# Try online with Jupyter Lite and Pyolite

With [Pyodide](https://github.com/pyodide/pyodide) [Jupyter Lite](https://jupyterlite.readthedocs.io) and a python kernel based on javascript you can now test ~95% of this tutorial without any local installation :
https://sed-rennes.gitlabpages.inria.fr/formations/introduction-to-python/lab/index.html

# Installation

To run notebooks of this tutorial you need:

* a installed python version 3.6 or newer (but 98% code are compatible with python version 3.3 )
* a Ipython notebook or jupyter notebook installed
* Numpy python module
* matplotlib


## On Linux 
### Ubuntu, Debian

```bash
sudo apt update
sudo apt install python3 python3-pip jupyter python3-numpy python3-matplotlib git
```

of with pip :

```bash
sudo apt update
sudo apt install python3-pip git
pip3 install jupyter numpy matplotlib
```
    
### Fedora
```bash
sudo dnf update
sudo dnf install python3 python3-pip git

pip3 install jupyter numpy matplotlib

```

### Windows
#### Install [WinPython](http://winpython.github.io/) 
version 3.6 or newer 64 bits is better in a folder path **without white-spaces** ( example: `c:\winpy-3.7-64x` )


### Install [git](https://git-scm.com/download/win)
set option to have git in path


## MacOS
* follow [Manual install step by step using Homebrew package manager](http://penandpants.com/2013/04/04/install-scientific-python-on-mac-os-x/)
     
* or follow a [more recent manual installation steps here](https://www.chrisjmendez.com/2019/04/20/install-numpy-scipy-matplotlab/) 
* you can also try  [Anaconda](http://docs.continuum.io/anaconda/)

them install with pip :

`python3 -m pip install jupyter numpy matplotlib`
or if your python version is 3.x :

`python --version`

run :

`pip install jupyter numpy matplotlib`

### git 
follow https://git-scm.com/download/mac


# get the tutorial notebooks and run jupyter :


## On Linux/MacOS:

open a console and run :

```
git clone https://gitlab.inria.fr/sed-rennes/formations/introduction-to-python.git
cd introduction-to-python
jupyter-notebook 

```


## On windows:

In file explorer go to winpython install folder (example `c:\winpy-3.7-64x` )

the run the executable "python console"

```
cd notebooks
git clone https://gitlab.inria.fr/sed-rennes/formations/introduction-to-python.git
```

In file explorer go to winpython install folder the run "jupyter-notebook" executable

## for all OS
 Jupyter-notebook run as a web server running on console, normally a web navigator will open a jupyter notebook page as a directory index with a URL similar to : `http://localhost:8889/tree`
 You can browser file to find the notebooks 



