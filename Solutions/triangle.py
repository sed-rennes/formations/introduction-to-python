# -*- coding: utf-8 -*-
"""
Some calculus of triangles

Created on Wed Jun 17 11:28:17 2015

@author: G.Andrade B. INRIA
"""

import math

def area_triagle(p1,p2,p3):
    '''Compute area of a triangle defined by 3 points in 2D plan

    >>> area_triagle((0,0),(0,1 ),(1,0))
    0.5
    '''
    dax=p2[0]-p1[0]
    day=p2[1]-p1[1]
    
    
    dbx=p3[0]-p1[0]
    dby=p3[1]-p1[1]

    return  abs(day*dbx-dax*dby)/2.0
  
  
def total_area(seq):
    """ Compute total area of a set of triangles in 2D plan

    >>> list_triangles=[((0,0),(0,1),(1,0)), ((0,1),(1,1),(1,0))]
    
    >>> total_area(list_triangles)
    1.0
    """
    return sum((area_triagle(*triangle) for triangle in seq))
        

if __name__ == "__main__":
    import doctest
    doctest.testmod()
