import unittest

def power2(x):
    return x*x

class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(power2(2), 4)
        
# creating a new test suite
newSuite = unittest.TestSuite()
 
# adding a test case
newSuite.addTest(unittest.makeSuite(MyTest))

if __name__ == "__main__":
    unittest.main()
