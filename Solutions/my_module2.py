def double(x):
    """Double x.

    >>> double(2)
    4
    >>> double(5)
    10
    """

    return 2*x

if __name__ == '__main__':
    import doctest
    doctest.testmod()
